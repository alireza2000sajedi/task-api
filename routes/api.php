<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {

    Route::post('signout', [AuthController::class, 'signout'])->name('signout');
    Route::get('user', [AuthController::class, 'user'])->name('user');
    //task
    Route::apiResource('task', TaskController::class);
    Route::put('task/{task}/toggle', [TaskController::class, 'toggle'])->name('task.toggle');

});
Route::post('signin', [AuthController::class, 'signin'])->name('signin');
Route::post('signup', [AuthController::class, 'signup'])->name('signup');
Route::get('user/{email}/exists', [AuthController::class, 'checkUserExistsByEmail'])->name('user.exists');
