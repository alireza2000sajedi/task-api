<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:app {name} {--admin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new app files';

    /**
     * The check admin.
     *
     * @var boolean
     */
    public $isAdmin = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Doing start...');
        $this->isAdmin = (boolean)$this->option('admin');
        $pluralName = Str::snake(Str::plural(class_basename($this->argument('name'))));
        $ufName = Str::ucfirst(Str::camel(class_basename($this->argument('name'))));
        if (!$this->isAdmin) {
            $this->makeModel($ufName);
            $this->makeMigration($pluralName);
            $this->makeRepository($ufName);
            $this->makeRequest($ufName);
        }
        $this->makeController($ufName);
        $this->makeResource($ufName);
        $this->info('Good luck :)');
    }

    private function makeModel($name)
    {
        $this->call('make:model', [
            'name' => "{$name}",
        ]);
    }

    private function makeController($name)
    {
        if (!$this->isAdmin) {
            $this->call('make:controller', [
                'name' => "{$name}Controller",
            ]);
        } else {
            $this->call('make:controller', [
                'name' => "Admin/Admin{$name}Controller",
            ]);
        }
    }

    private function makeRequest($name)
    {
        $this->call('make:request', [
            'name' => "{$name}Request",
        ]);
    }

    private function makeResource($name)
    {
        if (!$this->isAdmin) {
            $this->call('make:resource', [
                'name' => "{$name}Resource",
            ]);
        } else {
            $this->call('make:resource', [
                'name' => "Admin/Admin{$name}Resource",
            ]);
        }
    }

    private function makeMigration($name)
    {
        $this->call('make:migration', array_merge([
                                                      'name'     => "create_{$name}_table",
                                                      '--create' => $name,
                                                  ]));
    }

    private function makeRepository($name)
    {
        $this->call('make:repository', [
            'name'    => "{$name}Repository",
            '-i'
        ]);
    }
}
