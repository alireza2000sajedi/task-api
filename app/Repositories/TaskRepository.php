<?php

namespace App\Repositories;

use App\Models\Task;
use App\Repositories\Src\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class TaskRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Task::class;
    }

    public function create(array $data): Model
    {
        $this->unsetClauses();
        $data['user_id'] = auth()->id();
        return $this->model->create($data);
    }
}
