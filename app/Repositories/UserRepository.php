<?php

namespace App\Repositories;

use App\Repositories\Src\BaseRepository;
use App\Models\User;
use JetBrains\PhpStorm\ArrayShape;

class UserRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    #[ArrayShape(['token' => "mixed"])] public function token($user): array
    {
        $token = $user->createToken('auth');
        return ['token' => $token->plainTextToken];
    }

    public function findByEmail($email)
    {
        return $this->findBy('email', $email);
    }

    public function existsEmail($email)
    {
        return $this->newQuery()->query->where('email', $email)->exists();
    }
}
